import mlflow
import requests
import zipfile
import pandas as pd
import os


def fetch_mlflow_logged_data(run_id):
    """
    Returns parameters, metrics, tags, and artifacts of an mlflow run id.
    Args:
        run_id: The MLflow run id

    Returns:
        (list)

    """
    client = mlflow.tracking.MlflowClient()
    data = client.get_run(run_id).data
    tags = {k: v for k, v in data.tags.items() if not k.startswith("mlflow.")}
    artifacts = [f.path for f in client.list_artifacts(run_id, "model")]
    return data.params, data.metrics, tags, artifacts


def download_zip_file(url, save_path, chunk_size=128):
    r = requests.get(url, stream=True)
    with open(save_path, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=chunk_size):
            fd.write(chunk)


def unzip(path_to_zip_file, dir_to_extract_to):
    with zipfile.ZipFile(path_to_zip_file, 'r') as zip_ref:
        zip_ref.extractall(dir_to_extract_to)


def setup_tracking_database():
    try:
        TRACKING_URI = os.getenv('MLFLOW_TRACKING_URI')
        ARTIFACT_STORE = os.getenv('MLFLOW_ARTIFACT_STORE')
        store = db_store.SqlAlchemyStore(TRACKING_URI, ARTIFACT_STORE)
        return store
    except Exception as e:
        print(str(e))

