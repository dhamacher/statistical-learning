import sqlalchemy as sqldb
import os
import pandas as pd

""" Class in singelton pattern, to fetch data from PostgreSql."""
class PostgresEngine:
    __instance = None
    __engine = None

    @staticmethod 
    def getInstance(db: str):
        """
        Static access method to access postgres.

        Args:
            db: The database name.

        Returns:

        """

        if PostgresEngine.__instance == None:
            PostgresEngine(db)
        return PostgresEngine.__instance


    def __init__(self, db: str):
        """ Virtually private constructor. """
        if PostgresEngine.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            database_name = db
            POSTGRES_BASE_URI = os.getenv('POSTGRES_BASE_URI')

            DB_URI = f'{POSTGRES_BASE_URI}{database_name}'
            # psycopg2
            self.__engine = sqldb.create_engine(DB_URI)
            PostgresEngine.__instance = self


    def sql(self, query: str) -> pd.DataFrame:
        try:            
            data = pd.read_sql_query(query, self.__engine)
            return data
        except Exception as e:
            print(str(e))



