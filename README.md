# statistical-learning
Consisting of statistical learning labs


# Start MLflow server
In order to use the feature from MLflow such as the model registry, we need to do the following:
* Create a new environment variable named **MLFLOW_TRACKING_URI** and **MLFLOW_ARTIFACT_STORE**
    * For example, to use postgres use `postgresql+psycopg2://USER:PASSWORD@SERVER/DATABASE` for the value of **MLFLOW_TRACKING_URI**
    * For the **MLFLOW_ARTIFACT_STORE**, I used my local filesystem i.e. `file:/C:\mlflow\artifacts`
* Run the `start_mlflow.bat` script.