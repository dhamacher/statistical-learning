from pathlib import Path
from util.general import fetch_mlflow_logged_data, download_zip_file, unzip, setup_tracking_database
import pandas as pd
import numpy as np #780-508-9200
from sklearn import preprocessing
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.model_selection import cross_val_score
from sklearn.utils import resample
from sklearn import tree
from sklearn.pipeline import make_pipeline
from matplotlib import pyplot
import mlflow
from  mlflow.tracking import MlflowClient
import os

mlflow.set_tracking_uri(os.getenv('MLFLOW_TRACKING_URI'))
mlflow.set_experiment('Bank Direct Marketing')

# Paramters
k = 3       # Used for feature selector i.e. the k best features are selected.
RUN_NAME = f'Run with K = {k}'


def load_data() -> pd.DataFrame:
    ROOT_DIR = Path.cwd()
    DATA_DIR = ROOT_DIR / 'data'
    ZIP = Path.cwd() / 'tmp' / 'bank.zip'
    download_zip_file('https://archive.ics.uci.edu/ml/machine-learning-databases/00222/bank.zip', ZIP)
    unzip(ZIP, DATA_DIR)

    df = pd.read_csv(DATA_DIR / 'bank-full.csv', header=0, sep=';', quotechar='\"')
    return df


def get_feature_matrix(df: pd.DataFrame, x_cat_columns: list, x_num_columns: list, y_vector: np.ndarray, k_features: int) -> list:
    y = y_vector
    X_cat = df[x_cat_columns]
    X_num = df[x_num_columns].to_numpy()
    
    enc = preprocessing.OneHotEncoder()
    enc.fit(X_cat)
    X_cat_new = enc.transform(X_cat)
    
    feature_selector = SelectKBest(chi2, k=k_features)
    X_cat_select = feature_selector.fit_transform(X_cat_new, y)
    # X_num_select = feature_selector.fit_transform(X_num, y)

    ## what are scores for the features
    # for i in range(len(feature_selector.scores_)):
    #     print('Feature %d: %f' % (i, feature_selector.scores_[i]))
    # # plot the scores
    # pyplot.bar([i for i in range(len(feature_selector.scores_))], feature_selector.scores_)
    # pyplot.show()

    X = np.concatenate((X_cat_select.toarray(), X_num), axis=1)
    
    scaler = preprocessing.StandardScaler().fit(X)
    X_scaled = scaler.transform(X)
    return X_scaled


def encode_target(df, y_column: str):    
	le = preprocessing.LabelEncoder()
	le.fit(df.y)
	y_enc = le.transform(df.y)	
	return y_enc


# Load the Data
df = resample(load_data(), n_samples=4500)

# Get the features (X) and the target (y) from the dataset.
y = encode_target(df, 'y')
X = get_feature_matrix(df, 
                        ['job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'poutcome'], 
                        ['age', 'duration', 'campaign', 'previous'],
                        df.y, k)


with mlflow.start_run(run_name='bank-marketing'):
    client = MlflowClient()
    mlflow.sklearn.autolog()
    mlflow.log_param('select-best-k', k)
    # Create decision tree
    tree_m = tree.DecisionTreeClassifier()
    tree_scores = cross_val_score(tree_m, X, y, cv=5)

